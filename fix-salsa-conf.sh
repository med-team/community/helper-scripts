#!/bin/bash

# List all med-team repos and save them in a file
salsa --group med-team list_repos > all_repo_details.txt

#List out all names
grep 'Name:[[:space:]]' all_repo_details.txt > all_repo_names.txt

# Prefix with med-team
sed -i 's|Name:[[:space:]]|med-team\/|g' all_repo_names.txt

# Change ci config of every repo
for f in `cat all_repo_names.txt`
do
	salsa update_repo --ci-config-path debian/salsa-ci.yml $f
	echo "Changed ci config for $f"
done
